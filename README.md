# Industrie 4.0 : Séquence Communiquer

*Formation Industrie 4.0 / Ingénieur 2000 / Référent Anthony Baillard*

![](https://i.imgur.com/Zv6O1lP.png)

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) for [Ingénieurs2000](https://www.ingenieurs2000.com) - All rights reserved for educational purposes only*

---

<br>

# Définition du répertoire

Le contenu de ce répertoire à pour objectif de servir de base pour la connexion d'une page Web à un Broker MQTT dans l'objectif de mettre en place de systèmes de communications en temps réel.

- **Supports de cours** https://hackmd.io/@school-inge-2000/BJAT5IDNu
- **Lien vers la version finale** https://mqtt.dwsapp.io

<br>

## Utilisation du répertoire

Le projet contenu dans ce répertoire utilise des fonctionnalités asynchrones qui ne peuvent fonctionner que dans un environnement serveur, c'est pourquoi nous vous conseillons d'installer **LiveServer** sur votre machine avec la commande suivante : 

```
sudo npm i -g live-server
```

Il suffi ensuite de vous placer dans le dossier dans lequel vous souhaitez lancer votre environnement serveur et taper la commande suivante : 

```
live-server
```

---

<br><br><br>

# Ressources

*Liste de liens utiles pour la mise en place de la séquence*

![](https://i.imgur.com/eAySYs0.png)

- Node Red https://nodered.org/
- Paho https://www.eclipse.org/paho/index.php?page=clients/js/index.php
- HiveMQ https://www.hivemq.com

<br>

---

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) for [Ingénieurs2000](https://www.ingenieurs2000.com) - All rights reserved for educational purposes only*

---